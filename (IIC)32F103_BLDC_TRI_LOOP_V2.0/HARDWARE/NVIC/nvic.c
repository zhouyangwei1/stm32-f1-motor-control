#include "stm32f10x.h"
#include "misc.h"
#include "nvic.h"
#include "sys.h" 
#include "delay.h"
/*
中断分配说明
USART2     0-2     	接收上位机指令
TIM1       1-0    	高级定时器，生成PWM
TIM3			 2-2			串口发送数据
EXTI4，5   2-0;2-1 	按键

*/

//1.-----按键中断
void EXTI_KEY_NVIC_Config(NVIC_InitTypeDef NVIC_InitStructure1)
{
	/*
	NVIC_InitStructure1.NVIC_IRQChannel = EXTI0_IRQn;
	NVIC_InitStructure1.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure1.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure1.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure1);
	*/
  
	NVIC_InitStructure1.NVIC_IRQChannel = EXTI4_IRQn;
	NVIC_InitStructure1.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure1.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure1.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure1);
	
		NVIC_InitStructure1.NVIC_IRQChannel = EXTI9_5_IRQn;
	NVIC_InitStructure1.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure1.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure1.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure1);
}


//3----串口1通信，用于Modbus-Rtu-485S
void USART1_NVIC_Config(NVIC_InitTypeDef NVIC_InitStructure4)
{
	NVIC_InitStructure4.NVIC_IRQChannel = USART1_IRQn; 
	NVIC_InitStructure4.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure4.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure4.NVIC_IRQChannelCmd = ENABLE; 
	NVIC_Init(&NVIC_InitStructure4);
}

//4. 串口2
void USART2_NVIC_Config(NVIC_InitTypeDef NVIC_InitStructure3)
{
  //优先级设置高于定时器中断
	//Usart2 NVIC 配置
  NVIC_InitStructure3.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure3.NVIC_IRQChannelPreemptionPriority=0;//抢占优先级1
	NVIC_InitStructure3.NVIC_IRQChannelSubPriority =2;		//子优先级1
	NVIC_InitStructure3.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure3);	//根据指定的参数初始化VIC寄存器
}
//6.串口3
void USART3_NVIC_Config(NVIC_InitTypeDef NVIC_InitStructure3)
{
  //优先级设置高于定时器中断
	//Usart2 NVIC 配置
  NVIC_InitStructure3.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure3.NVIC_IRQChannelPreemptionPriority=1;//抢占优先级1
	NVIC_InitStructure3.NVIC_IRQChannelSubPriority =1;		//子优先级1
	NVIC_InitStructure3.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure3);	//根据指定的参数初始化VIC寄存器
}
//5 定时器3
void TIM3_NVIC_Config(NVIC_InitTypeDef NVIC_InitStructure3)
{
  //优先级设置低于串口中断
	//Usart2 NVIC 配置
  NVIC_InitStructure3.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure3.NVIC_IRQChannelPreemptionPriority=2;//抢占优先级1
	NVIC_InitStructure3.NVIC_IRQChannelSubPriority =2;		//子优先级1
	NVIC_InitStructure3.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure3);	//根据指定的参数初始化VIC寄存器
}
//7 定时器2
void TIM2_NVIC_Config(NVIC_InitTypeDef NVIC_InitStructure3)
{
  NVIC_InitStructure3.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure3.NVIC_IRQChannelPreemptionPriority=1;//抢占优先级1
	NVIC_InitStructure3.NVIC_IRQChannelSubPriority =1;		//子优先级1
	NVIC_InitStructure3.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure3);	//根据指定的参数初始化VIC寄存器
}
//2 定时器4
void TIM4_NVIC_Config(NVIC_InitTypeDef NVIC_InitStructure2)
{
	NVIC_InitStructure2.NVIC_IRQChannel = TIM4_IRQn; 
	NVIC_InitStructure2.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure2.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure2.NVIC_IRQChannelCmd = ENABLE; 
	NVIC_Init(&NVIC_InitStructure2);

}

//8 定时器1
void TIM1_NVIC_Config(NVIC_InitTypeDef NVIC_InitStructure2)
{
	NVIC_InitStructure2.NVIC_IRQChannel = TIM1_UP_IRQn; 
	NVIC_InitStructure2.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure2.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure2.NVIC_IRQChannelCmd = ENABLE; 
	NVIC_Init(&NVIC_InitStructure2);

}

void NVIC_Config(u8 Interrupt_flag)
{ 

  	NVIC_InitTypeDef   NVIC_InitStructure;
	/* 配置中断使用组合  抢占式3位(0-7)，响应式1位(0-1) */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_3);

	if(Interrupt_flag == 1)
	{
		/*外部中断1，Z相归零----优先级最高 0 0*/
		EXTI_KEY_NVIC_Config(NVIC_InitStructure);
	}
	
	if(Interrupt_flag == 2)
	{	
		/*TIM4中断，编码器解码----优先级 1  1*/
		TIM4_NVIC_Config(NVIC_InitStructure);
	}
	if(Interrupt_flag == 3)
	{
		/* USART1中断，数据接收----优先级 2  1*/
		USART1_NVIC_Config(NVIC_InitStructure);
	}
	if(Interrupt_flag == 4)
	{
		/* USART2中断，数据接收----优先级 0  1*/
		USART2_NVIC_Config(NVIC_InitStructure);
	}
	if(Interrupt_flag == 5)
	{	
		/*TIM3中断�1*/
		TIM3_NVIC_Config(NVIC_InitStructure);
	}
	if(Interrupt_flag == 6)
	{	
		/*USART3中断,*/
		USART3_NVIC_Config(NVIC_InitStructure);
	}
	if(Interrupt_flag == 7)
	{	
		/*TIM2中断,*/
		TIM2_NVIC_Config(NVIC_InitStructure);
	}
	if(Interrupt_flag == 8)
	{	
		/*TIM1中断,*/
		TIM1_NVIC_Config(NVIC_InitStructure);
	}

}


