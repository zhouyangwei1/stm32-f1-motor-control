#ifndef __CRC16_H
#define __CRC16_H

#include <stdint.h>

int  crc16_modbus(char *puchMsg, char usDataLen);

#endif
